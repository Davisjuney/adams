<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// list articles 
Route::get('/articles', 'ArticlesController@index');
// show single article 
Route::get('/article/{id}', 'ArticlesController@show');
// create articles 
Route::post('/article', 'ArticlesController@store');
// update articles 
Route::put('/article', 'ArticlesController@store');
// delete articles 
Route::delete('/article/{id}', 'ArticlesController@destroy');