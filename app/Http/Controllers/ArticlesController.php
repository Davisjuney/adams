<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Article;
use App\Http\Resources\Article as ArticleResource;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::orderBy('created_at','desc')->paginate(5);
        // return articles as resource collection
        return ArticleResource::collection($articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article = $request->isMethod('put') ? Article::findOrFail($request->article_id) : new Article;

        // handle file upload
       if ($request->hasFile('cover_image'))
        {
        $fileNameWithExt = $request->file('cover_image')->getClientOriginalName();
        // get just filename
        $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

        // get just extension
         $extension = $request->file('cover_image')->getClientOriginalExtension();

        $fileNameToStore = $filename. '_'. date(). '.'. $extension;

           // upload image 
       $path= $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);
        } else
        {
          $fileNameToStore = 'noimage.jpg' ;

        }

  
        // store or update article 
        $article->title = $request->input('title');
        $article->description = $request->input('description');
        $article->cover_images = $fileNameToStore;
         $article->id = $request->input('id');
        if ($article->save()) {
            return new ArticleResource($article);
        }
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::findOrFail($id);
        // return found article as resource 
        return new ArticleResource($article);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        $article = Article::findOrfail($id);
        $retVal= $article->delete() ? new ArticleResource($article) : 'unable to delete article';
        return $retVal;
        
    }
}
