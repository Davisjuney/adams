<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Article extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       // return parent::toArray($request);
        return [
            'title' => $this->title,
            'description' => $this->description,
            'id' => $this->id,
            'image' => $this->cover_images
        ];
    }
    public function with($request)
    {
        return [
            'version' => '123.568.65',
            'creator' => 'Davis Juney',
            'creator profile' => url('www.facebook.com/thavid.king'),
            'website' => 'adams.proj'
        ];
    }
}
